/*
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.dth.mdr.validator;

import java.util.List;

import de.samply.common.mdrclient.domain.ErrorMessage;

public class ValidationError {
    private String mdrKey;
    private String dthKey;
    private String value;
    private int line;
    private List<ErrorMessage> errorMessages;
    
    public String toString() {
        return "DTHKey= '"+dthKey+"'\nValue= " +
                "'"+value+"'\nLine='"+line+"'\nMdrKey='"+mdrKey+"'\nErrorMessage='"+
                (errorMessages==null||errorMessages.isEmpty()?"":errorMessages.get(0).getDesignation()+" -> "+errorMessages.get(0).getDefinition()+"'");
    }
    
    
    public String getMdrKey() {
        return mdrKey;
    }
    public void setMdrKey(String mdrKey) {
        this.mdrKey = mdrKey;
    }
    public String getDthKey() {
        return dthKey;
    }
    public void setDthKey(String dthKey) {
        this.dthKey = dthKey;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public List<ErrorMessage> getErrorMessages() {
        return errorMessages;
    }
    public void setErrorMessages(List<ErrorMessage> errorMessages) {
        this.errorMessages = errorMessages;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }
}
