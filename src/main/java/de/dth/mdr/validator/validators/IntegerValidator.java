/*
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.dth.mdr.validator.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.dth.mdr.validator.exception.ValidatorException;
import de.samply.common.mdrclient.domain.Validations;

/**
 * Validator for integers.
 */
public class IntegerValidator extends Validator {
    
    /**
     * A integer regular expression.
     */
    private static final String DIGIT_ONLY_REGEX = "^[0-9]*$";

    /**
     * The pattern of a integer regular expression.
     */
    private Pattern pattern;

    /**
     * Create a new integer validator.
     */
    public IntegerValidator() {
        pattern = Pattern.compile(DIGIT_ONLY_REGEX);
    }

    @Override
    public final boolean validate(Validations dataElementValidations, final Object value)
            throws ValidatorException {
        if (value == null || value.toString().equals("")) {
            return true;
        }

        String checkMe = value.toString();
        if(getUnitOfMeasure() != null) {
            checkMe = cutUnitOfMeasure(checkMe);
        }

        Matcher matcher = pattern.matcher(checkMe);
        return matcher.matches();
    }
}