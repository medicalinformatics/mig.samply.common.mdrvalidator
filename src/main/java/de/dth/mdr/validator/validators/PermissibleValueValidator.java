/*
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.dth.mdr.validator.validators;

import de.dth.mdr.validator.exception.ValidatorException;
import de.samply.common.mdrclient.domain.PermissibleValue;
import de.samply.common.mdrclient.domain.Validations;

/**
 * Validator for permissible values.
 */
public class PermissibleValueValidator extends Validator {
    @Override
    public final boolean validate(Validations dataElementValidations, final Object value) throws ValidatorException {

        if (value == null || value.toString().equals("")) {
            return true;
        }

        // no permissible values. should not happen, but if so, the element is always valid
        if(dataElementValidations.getPermissibleValues().size() <= 0)
            return true;

        if (isCaseSensitive()) {
            return validateCaseSensitive(dataElementValidations, value);
        } else {
            return validateIgnoreCase(dataElementValidations, value);
        }
    }

    private boolean validateCaseSensitive(Validations dataElementValidations, final Object value) {
        for (PermissibleValue permissibleValue : dataElementValidations.getPermissibleValues()) {
            if (permissibleValue.getValue().equals(value.toString()))
                return true;
        }

        return false;
    }

    private boolean validateIgnoreCase(Validations dataElementValidations, final Object value) {
        for (PermissibleValue permissibleValue : dataElementValidations.getPermissibleValues()) {
            if (permissibleValue.getValue().equalsIgnoreCase(value.toString()))
                return true;
        }

        return false;
    }
}