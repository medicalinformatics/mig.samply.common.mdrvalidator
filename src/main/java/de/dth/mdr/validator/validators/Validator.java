/*
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.dth.mdr.validator.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Validator implements ValidatorInterface {
    private String unitOfMeasure;

    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    private boolean caseSensitive;

    public Validator(){
        caseSensitive = true;
    }

    /**
     * Cuts the unit of measure from a given string (and all whitespaces)
     * and returns the pure number. It does not check if that number is actually a number!
     * If no unit of measure is found, just returns the given string
     *
     * @param value a number (unchecked) maybe including the defined unit of measure
     * @return  the value without the unit of measure
     */
    protected String cutUnitOfMeasure(String value) {
        // get number before unit of measure
        String unitOfMeasurePatternString = "^\\s*(\\S*)\\s*"+getUnitOfMeasure()+"\\s*$";
        Pattern unitOfMeasurePattern = Pattern.compile(unitOfMeasurePatternString);
        Matcher unitOfMeasureMatcher = unitOfMeasurePattern.matcher(value);

        // if the unit of measure is found, just check the number,
        // if not, we allow that the value is saved without the unit of measure, too
        if(unitOfMeasureMatcher.matches()) {
            value = unitOfMeasureMatcher.group(1);
        }

        return value;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

}
