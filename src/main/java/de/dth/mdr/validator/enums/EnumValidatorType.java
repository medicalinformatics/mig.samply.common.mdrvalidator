/*
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.dth.mdr.validator.enums;

/**
 * Enum for the different validator types
 */
public enum EnumValidatorType {
    /**
     * No validation is supported or validation do not apply.
     */
    NONE,
    /**
     * Data should be validated as a boolean.
     */
    BOOLEAN,
    /**
     * Data should be validated as an integer.
     */
    INTEGER,
    /**
     * Data should be validated as a float.
     */
    FLOAT,
    /**
     * Data should be an integer in between a minimum and maximum value.
     */
    INTEGERRANGE,
    /**
     * Data should be an float in between a minimum and maximum value.
     */
    FLOATRANGE,
    /**
     * Data should be validated as a date.
     */
    DATE,
    /**
     * Data should be validated as time.
     */
    TIME,
    /**
     * Data should be validated as a date and time value.
     */
    DATETIME,
    /**
     * Data should be validated with a regular expression.
     */
    REGEX,
    /**
     * Data validated per catalogue
     */
    CATALOG,
    /**
     * Data validated per enum/permissible values
     */
    ENUMERATED,
    /**
     * Data validated as String (with possible maximum character quantity)
     */
    STRING
}

