/*
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.dth.mdr.validator.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

import de.dth.mdr.validator.MDRValidator;
import de.dth.mdr.validator.MdrConnection;
import de.dth.mdr.validator.exception.MdrException;
import de.dth.mdr.validator.exception.ValidatorException;
import de.samply.common.config.Configuration;
import de.samply.common.config.ObjectFactory;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.ErrorMessage;
import de.samply.config.util.FileFinderUtil;
import de.samply.config.util.JAXBUtil;

public class MDRValidatiorTest {
    private static final String mdrURL = "https://mdr-dth.mitro.dkfz.de/v3/api/mdr";
    private static final String authUserId = "20";
    private static final String keyId = "2";
    private static final String authURL = "https://login.mitro.dkfz.de";
    private static final String privateKeyBase64 = "";
    private static final String nameSpace = "dth";

    private static MdrConnection mdrConnection;

    @BeforeClass
    public static void init() throws JAXBException, SAXException, ParserConfigurationException, IOException, URISyntaxException {
        System.setProperty("jsse.enableSNIExtension", "false");

        String filePath = MDRValidatiorTest.class.getResource("/").getFile();
        
        File configFile = FileFinderUtil.findFile("proxy.xml", "dth.validator", filePath);
        assertNotNull(configFile);
        
        Configuration proxyConfiguration = JAXBUtil.unmarshall(configFile,
                JAXBContext.newInstance(ObjectFactory.class), Configuration.class);
        
        mdrConnection = new MdrConnection(mdrURL, authUserId, keyId, authURL, privateKeyBase64, nameSpace, proxyConfiguration, true);
    }
    
    @AfterClass
    public static void end() {
        mdrConnection.close();
    }
    
    @Test
    public void test() throws MdrConnectionException, MdrInvalidResponseException, ExecutionException, MdrException, ValidatorException {
        MDRValidator val = new MDRValidator(mdrConnection, "urn:dth:dataelementgroup:8:latest");
        assertNotNull(val);

        // element urn:dth:dataelement:151:2 is a float range 0.0<=x<=100.0 with % as unit of measure
        String mdrKey = "urn:dth:dataelement:151:2";

        boolean validated = val.validate(mdrKey, "1.06%");
        assertTrue(validated);
        if(!validated) {
            for (ErrorMessage error : val.getErrorMessage(mdrKey)) {
                System.out.println(error.getDesignation() + " : " + error.getDefinition());
            }
        }

        // also allows values without the unit of measure
        assertTrue(val.validate(mdrKey, "1.06"));

        // out of range, so should be false
        assertFalse(val.validate(mdrKey, "900.06%"));

//        // slot stuff test
//        val = new DTHValidator(mdrConnection, "urn:onkostar:dataelementgroup:12:latest");
//        mdrKey = "urn:onkostar:record:38:1";
//
//        System.out.println(val.getMdrKeyOfSlot("OnkostarName", "dTNMTpraefix"));
    }
}
